#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ---------------------------------------------------------------------
# Compendio de programas.
# Matemáticas para Ingeniería. Métodos numéricos con Python.
# Copyright (C) 2020 Los autores del texto.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ---------------------------------------------------------------------

# Implementación de la regla del trapecio y algunos casos de salida.

from math import *


def pol(x):
    """Función de prueba"""
    return x**3 + 4*x**2 - 10  # retorna $pol(x)=x^3+4x^2-10$


def trig(x):
    """Función de prueba"""
    return x*cos(x-1) - sin(x)  # retorna $trig(x)=x\cos(x-1)-\sin(x)$


def trapecio(f, a, b, n):
    """
    Implementación regla del trapecio
    Entradas:
    f -- función
    a -- inicio intervalo
    b -- fin intervalo
    n -- número de pasos

    Salida:
    abc -- aproximación área bajo la curva
    """
    h = (b - a)/n
    acum = 0
    for j in range(1, n):
        acum += 2*f(a + h*j)
    abc = (h/2)*(f(a) + acum + f(b))
    return abc


# $pol(x)$, $a = 1$, $b = 2$, $N = 10$
print("\nÁrea bajo la curva pol(x):\n")
print("{0:.12f}".format(trapecio(pol, 1, 2, 10)))

# $trig(x)$, $a = 4$, $b = 6$, $N = 20$
print("\nÁrea bajo la curva trig(x):\n")
print("{0:.12f}".format(trapecio(trig, 4, 6, 20)))
