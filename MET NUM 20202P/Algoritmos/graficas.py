#### Grafica de funciones
def graficar(list, x_i, x_f, int=1000):
    import matplotlib.pyplot as plt
    import numpy as np
    fig, ax = plt.subplots()
    x = np.linspace(x_i, x_f, int)
    for f in list:
        ax.plot(x, f(x))
    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()
    ax.grid(True, linestyle='-')
    ax.annotate("", xy=(xmax, 0), xytext=(xmin, 0),
                arrowprops=dict(color='gray', width=1.5, headwidth=8, headlength=10))
    ax.annotate("", xy=(0, ymax), xytext=(0, ymin),
                arrowprops=dict(color='gray', width=1.5, headwidth=8, headlength=10))
    plt.show()


def graficar_puntos(list, x_i, x_f, datos, int=1000):
    import matplotlib.pyplot as plt
    import numpy as np
    fig, ax = plt.subplots()
    x = np.linspace(x_i, x_f, int)
    datosx = [dato[0] for dato in datos]
    datosy = [dato[1] for dato in datos]
    for f in list:
        ax.plot(x, f(x))
    ax.plot(datosx, datosy, 'ko')
    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()
    ax.grid(True, linestyle='-')
    ax.annotate("", xy=(xmax, 0), xytext=(xmin, 0),
                arrowprops=dict(color='gray', width=1.5, headwidth=8, headlength=10))
    ax.annotate("", xy=(0, ymax), xytext=(0, ymin),
                arrowprops=dict(color='gray', width=1.5, headwidth=8, headlength=10))
    plt.show()


def coeficientes_pol(F, n_datos):
    import numpy as np
    mA = list()
    b = list()
    for i in range(n_datos - 1):
        b.append(F(i + 1) - F(0))
        renglon_A = list()
        j = n_datos - 1
        while j > 0:
            renglon_A.append((i + 1) ** (j))
            j += -1
        mA.append(renglon_A)
    mA = np.array(mA)
    b = np.array(b)
    mA_inv = np.linalg.inv(mA)
    coefientes = mA_inv @ b
    coefientes = list(coefientes)
    coefientes.append(F(0))
    return coefientes
