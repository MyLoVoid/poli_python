#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ---------------------------------------------------------------------
# Compendio de programas.
# Matemáticas para Ingeniería. Métodos numéricos con Python.
# Copyright (C) 2020 Los autores del texto.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ---------------------------------------------------------------------

# Implementación del método de bisección y algunos casos de salida.

from math import *


def pol(x):
    """Función de prueba"""
    return x**3 + 4*x**2 - 10  # retorna $pol(x)=x^3+4x^2-10$


def trig(x):
    """Función de prueba"""
    return x*cos(x-1) - sin(x)  # retorna $trig(x)=x\cos(x-1)-\sin(x)$


def bisec(f, a, b, tol, n):
    """
    Implementación método de bisección
    Entradas:
    f -- función
    a -- inicio intervalo
    b -- fin intervalo
    tol -- tolerancia
    n -- número máximo de iteraciones

    Salida:
    p aproximación a cero de f
    None en caso de iteraciones agotadas
    """
    i = 1
    while i <= n:
        p = a + (b - a)/2
        print("i = {0:<2}, p = {1:.12f}".format(i, p))
        if abs(f(p)) <= 1e-15 or (b - a)/2 < tol:
            return p
        i += 1
        if f(a)*f(p) > 0:
            a = p
        else:
            b = p
    print("Iteraciones agotadas: Error!")
    return None


# $pol(x)$, $a = 1$, $b = 2$, $TOL = 10^{-8}$, $N_0 = 100$
print("Bisección función pol(x):")
bisec(pol, 1, 2, 1e-8, 100)

# $trig(x)$, $a = 4$, $b = 6$, $TOL = 10^{-8}$, $N_0 = 100$
print("Bisección función trig(x):")
bisec(trig, 4, 6, 1e-8, 100)
