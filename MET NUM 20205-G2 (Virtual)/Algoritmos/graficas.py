#### Grafica de funciones
def graficar(f, x_i, x_f, int = 1000):
    import matplotlib.pyplot as plt
    import numpy as np
    fig, ax = plt.subplots()
    x = np.linspace(x_i, x_f, int)
    ax.plot(x, f(x))
    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()
    ax.grid(True, linestyle='-')
    ax.annotate("", xy=(xmax, 0), xytext=(xmin, 0),
                arrowprops=dict(color='gray', width=1.5, headwidth=8, headlength=10))
    ax.annotate("", xy=(0, ymax), xytext=(0, ymin),
                arrowprops=dict(color='gray', width=1.5, headwidth=8, headlength=10))
    plt.show()
    
    
def graficar_puntos(f, x_i, x_f, datos, int = 1000):
    import matplotlib.pyplot as plt
    import numpy as np
    fig, ax = plt.subplots()
    x = np.linspace(x_i, x_f, int)
    datosx = [dato[0] for dato in datos]
    datosy = [dato[1] for dato in datos]
    ax.plot(x, f(x))
    ax.plot(datosx,datosy,'ko')
    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()
    ax.grid(True, linestyle='-')
    ax.annotate("", xy=(xmax, 0), xytext=(xmin, 0),
                arrowprops=dict(color='gray', width=1.5, headwidth=8, headlength=10))
    ax.annotate("", xy=(0, ymax), xytext=(0, ymin),
                arrowprops=dict(color='gray', width=1.5, headwidth=8, headlength=10))
    plt.show()